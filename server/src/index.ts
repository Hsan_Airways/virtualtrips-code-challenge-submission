import { AppDataSource } from './data-source'
import app from './app'
import dotenv from 'dotenv'
dotenv.config()
const PORT = process.env.PORT || 3001

AppDataSource.initialize()
	.then(async () => {
		app.listen(PORT)
		console.log(`RUNNING ON http://localhost:${PORT}`)
	})
	.catch((error) => console.log(error))
