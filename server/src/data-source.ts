import "reflect-metadata"

import { DataSource } from "typeorm"
import { Location } from "./entity/Location"

export const AppDataSource = new DataSource({
	type: "sqlite",
	database: 'data.sqlite',
	synchronize: true,
	logging: false,
	entities: [Location],
	migrations: [`${__dirname}/migration/**/*.ts`],
	subscribers: [],
})
