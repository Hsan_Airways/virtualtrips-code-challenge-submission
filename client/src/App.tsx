import React, { useCallback, useEffect, useState } from 'react'

import { searchLocations } from './api/search'
import useDebounce from './hooks/use-debounce'

function App() {
	const [query, setQuery] = useState<string>('')
	const [noResults, setNoResults] = useState<boolean>(false)
	const [error, setError] = useState<boolean>(false)
	const [results, setResults] = useState<string[]>([])
	const debouncedQueryValue = useDebounce(query, 500)

	const onChangeQuery = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
		setQuery(event.target.value)
	}, [])

	const fetchResults = async (query: string) => {
		setNoResults(false)
		setError(false)

		try {
			const locations = await searchLocations(query)
			setResults(locations)
			setNoResults(locations.length === 0)
		} catch (error) {
			setError(true)
			setResults([])
		}
	}

	useEffect(() => {
		if (debouncedQueryValue.length > 1) {
			fetchResults(debouncedQueryValue)
		} else {
			setResults([])
			setError(false)
			setNoResults(false)
		}
	}, [debouncedQueryValue])

	return (
		<div className='flex flex-col items-center w-full h-screen p-4 bg-gray-100'>
			<div className='flex flex-col justify-center w-full p-4 bg-white rounded shadow md:w-1/2'>
				<input
					type='text'
					className='w-full p-4 transition-colors duration-500 ease-in-out bg-gray-200 rounded hover:bg-gray-100 focus:bg-gray-100 focus:outline-none focus:ring-1 focus:ring-gray-200'
					placeholder='Start typing'
					value={query}
					onChange={onChangeQuery}
				/>
				{noResults && <span className='mt-2'>Nothing matches your query :(</span>}
				{error && <span className='mt-2'>Something went wrong :(</span>}
				<ul data-testid='results' className='w-full'>
					{results.map((location, index) => (
						<li
							key={String(index)}
							className='w-full p-3 mt-2 bg-gray-100 rounded cursor-pointer hover:bg-blue-50'
						>
							<span>{location}</span>
						</li>
					))}
				</ul>
			</div>
		</div>
	)
}

export default App
